import 'package:stacked/stacked.dart';
import 'package:stacked_example/app/locator.dart';
import 'package:stacked_example/services/counterService.dart';

class DoubleIncreaseCounterViewModel extends ReactiveViewModel{
  final _counterService =locator<CounterService>();
  int get counter => _counterService.counter;

  void updateCounter(){
    _counterService.doubleCounter();
    notifyListeners();
  }

  @override
  // TODO: implement reactiveServices
  List<ReactiveServiceMixin> get reactiveServices => [_counterService];
}