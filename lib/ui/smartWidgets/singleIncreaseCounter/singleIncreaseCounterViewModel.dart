import 'package:stacked/stacked.dart';
import 'package:stacked_example/app/locator.dart';
import 'package:stacked_example/services/counterService.dart';

class SingleIncreaseCounterViewModel extends ReactiveViewModel{
  final _counterService =locator<CounterService>();
  int get counter => _counterService.counter;

  void updateCounter(){
    _counterService.incrementCounter();
    notifyListeners();
  }

  @override
  // TODO: implement reactiveServices
  List<ReactiveServiceMixin> get reactiveServices => [_counterService];
}