import 'package:flutter/material.dart';

import 'package:stacked/stacked.dart';
import 'package:stacked_example/ui/views/futureExample/futureExampleViewModel.dart';

class FutureExampleView extends StatelessWidget {
  const FutureExampleView({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return ViewModelBuilder<FutureExampleViewModel>.reactive(
      builder: (context, model, child) {
        return Scaffold(
          body: Center(
              child: model.isBusy
                  ? CircularProgressIndicator()
                  : Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
                      Text(model.data ?? 'No_Data'),
                      _ErrorMessage(),
                    ])),
        );
      },
      viewModelBuilder: () => FutureExampleViewModel(),
    );
  }
}

class _ErrorMessage extends ViewModelWidget<FutureExampleViewModel> {
  const _ErrorMessage({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context, FutureExampleViewModel model) {
    return model.hasError
        ? Text(
            model.modelError.message,
            style: TextStyle(color: Colors.red),
          )
        : Container();
  }
}
