import 'package:stacked/stacked.dart';

class FutureExampleViewModel extends FutureViewModel<String> {
  Future<String> getDataFromServer  () async{
    await Future.delayed(const Duration(seconds: 2));
    throw Exception('ERROR !!!!!');
  }

  @override
  Future<String> futureToRun()=> getDataFromServer();

  @override
  void onError(error){
  }
}
