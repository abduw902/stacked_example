import 'package:stacked/stacked.dart';
import 'package:stacked_example/app/locator.dart';
import 'package:stacked_example/services/postsService.dart';

class PostCountViewModel extends BaseViewModel{
  int get postsCount=>locator<PostsService>().posts.length;
}