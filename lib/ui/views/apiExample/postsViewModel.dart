import 'package:stacked/stacked.dart';
import 'package:stacked_example/app/locator.dart';
import 'package:stacked_example/datamodels/post.dart';
import 'package:stacked_example/services/apiService.dart';

class PostsViewModel extends FutureViewModel<List<Post>> {
  final int userId;
  PostsViewModel(this.userId);

  @override
  Future<List<Post>> futureToRun()=> locator<Api>().getPostsForUser(userId);
}
