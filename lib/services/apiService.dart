import 'dart:convert';

import 'package:injectable/injectable.dart';
import 'package:stacked_example/datamodels/comment.dart';
import 'package:http/http.dart' as http;
import 'package:stacked_example/datamodels/post.dart';
import 'package:stacked_example/datamodels/user.dart';

@lazySingleton
class Api {
  static const url = 'https://jsonplaceholder.typicode.com';
  var client = new http.Client();

  Future<List<Comment>> getCommentsForPost(int postId) async {
    var comments = List<Comment>();
    var response = await client.get('$url/comments?postId=$postId');
    var parsed = json.decode(response.body) as List<dynamic>;
    for (var comment in parsed) {
      comments.add(Comment.fromJson(comment));
    }
    return comments;
  }

  Future<List<Post>> getPostsForUser(int userId) async {
    var posts = List<Post>();
    var response = await client.get('$url/posts?userId=$userId');
    var parsed = json.decode(response.body) as List<dynamic>;
    for (var post in parsed) {
      posts.add(Post.fromJson(post));
    }

    return posts;
  }

  Future<User> getUserProfile(int userId) async {
    var response = await client.get('$url/users/$userId');
    return User.fromJson(json.decode(response.body));
  }
}
