import 'package:flutter/material.dart';
import 'package:stacked_example/app/locator.dart';
import 'package:stacked_example/app/router.gr.dart' as auto_router;
import 'package:stacked_example/ui/views/futureExample/futureExampleView.dart';
import 'package:stacked_example/ui/views/home/homeView.dart';
import 'package:stacked_example/ui/views/postsExample/postsView.dart';
import 'package:stacked_example/ui/views/reactiveExample/reactiveExampleView.dart';
import 'package:stacked_example/ui/views/streamExample/streamExampleView.dart';
import 'package:stacked_services/stacked_services.dart';

void main() {
  setupLocator();
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter Demo',
      //initialRoute: auto_router.Routes.startupView,
      home: HomeView(),
      onGenerateRoute: auto_router.Router().onGenerateRoute,
      navigatorKey: locator<NavigationService>().navigatorKey,
    );
  }
}
