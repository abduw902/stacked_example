import 'package:auto_route/auto_route_annotations.dart';
import 'package:stacked_example/ui/views/home/homeView.dart';
import 'package:stacked_example/ui/views/startup/startupView.dart';

@MaterialAutoRouter(routes: [
  MaterialRoute(page: StartupView, initial: true),
  MaterialRoute(page: HomeView)
])
class $Router {}
