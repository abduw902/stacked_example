// GENERATED CODE - DO NOT MODIFY BY HAND

// **************************************************************************
// InjectableConfigGenerator
// **************************************************************************

import 'package:get_it/get_it.dart';
import 'package:injectable/injectable.dart';
import 'package:stacked_services/stacked_services.dart';

import '../services/apiService.dart';
import '../system/appDatabase.dart';
import '../services/counterService.dart';
import '../services/mediaService.dart';
import '../services/permissionsService.dart';
import '../services/postsService.dart';
import '../ui/views/postsExample/postsViewModel.dart';
import '../services/sharedPreferencesService.dart';
import '../services/thirdPartyServicesModule.dart';

/// adds generated dependencies
/// to the provided [GetIt] instance

GetIt $initGetIt(
  GetIt get, {
  String environment,
  EnvironmentFilter environmentFilter,
}) {
  final gh = GetItHelper(get, environment, environmentFilter);
  final thirdPartyServicesModule = _$ThirdPartyServicesModule();
  gh.lazySingleton<Api>(() => Api());
  gh.lazySingleton<AppDatabase>(() => AppDatabase());
  gh.lazySingleton<CounterService>(() => CounterService());
  gh.lazySingleton<DialogService>(() => thirdPartyServicesModule.dialogService);
  gh.lazySingleton<MediaService>(() => MediaService());
  gh.lazySingleton<NavigationService>(
      () => thirdPartyServicesModule.navigationService);
  gh.lazySingleton<PermissionsService>(() => PermissionsService());
  gh.lazySingleton<PostsService>(() => PostsService());
  gh.lazySingleton<SharedPreferencesService>(() => SharedPreferencesService());

  // Eager singletons must be registered in the right order
  gh.singleton<PostsViewModel>(PostsViewModel());
  return get;
}

class _$ThirdPartyServicesModule extends ThirdPartyServicesModule {
  @override
  DialogService get dialogService => DialogService();
  @override
  NavigationService get navigationService => NavigationService();
}
